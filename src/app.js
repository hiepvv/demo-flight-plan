let express = require('express')
let bodyParser = require('body-parser')
let db = require('./../models/index')

var userController = require('./../routes/users')
var authenController = require('./../routes/authen')

let app = express()
app.set('port', process.env.PORT || '8080')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/api', function (req, res){
  res.status(200).send('Ok2')
})
app.use('/api/users', userController)
app.use('/api/login', authenController)



module.exports = app