let express = require('express')
let router = express.Router()
let bodyParser = require('body-parser')

let User = require('./../models/user')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

router.post('/', function (req, res) {
  let info = {
    email: req.body.email || null,
    password: req.body.password || null,
  }

  if (!info.email || !info.password)
    return res.status(400).send("Bad request.")

  User.findOne({ email: info.email }, function (err, user) {
    if (err) return res.status(500).send("There was a problem.")
    if (user) return res.status(400).send("User was exist.")
  })

  User.create({
    email: req.body.email,
    password: req.body.password
  }, function (err, user) {
    if (err) return res.status(500).send("There was a problem adding the information to the database.")
    res.status(200).send(user)
  })
})
router.get('/:id', function (req, res) {
  User.findById(req.params.id, function (err, user) {
    if (err) return res.status(500).send("There was a problem finding the user.")
    if (!user) return res.status(404).send("No user found.")
    res.status(200).send(user)
  })
})

module.exports = router
