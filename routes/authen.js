let jwt = require('jsonwebtoken')
let express = require('express')
let router = express.Router()
let bodyParser = require('body-parser')

let User = require('./../models/user')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

router.post('/', function (req, res) {
  let info = {
    email: req.body.email || null,
    password: req.body.password || null,
  }

  if (!info.email || !info.password)
    return res.status(400).send("Bad request.")

  User.findOne({ email: info.email }, function (err, user) {
    if (err) return res.status(500).send("There was a problem.")
    if (!user) return res.status(400).send("User not found.")

    let token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (5 * 60),
      data: user
    }, 'secret')

    res.status(200).send(token)
  })
})

module.exports = router
